# xfce4-windowck-plugin

An Xfce panel plugin (used in UnityX) which allows putting buttons, title and menu of active or maximized windows on the panel.
This code is derived from [Window Applets](https://www.gnome-look.org/p/1115400) by Andrej Belcijan.

### Installation

```
./autogen.sh
make -j`nproc` # remove -j`nproc` for single-threaded builds
make install
```