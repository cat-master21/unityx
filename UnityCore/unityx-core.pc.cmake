prefix=@PREFIXDIR@
exec_prefix=@EXEC_PREFIX@
libdir=@LIBDIR@
includedir=@INCLUDEDIR@

Name: UnityX Core
Library
Description: Core objects and utilities for UnityX implementations
Version: @VERSION@
Libs: -L${libdir} -lunityx-core-@UNITY_API_VERSION@
Cflags: -I${includedir}/UnityX-@UNITY_API_VERSION@
Requires: glib-2.0 gio-2.0 sigc++-2.0 nux-core-4.0 dee-1.0
