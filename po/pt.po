# Portuguese translations for l package.
# Copyright (C) 2010 THE l'S COPYRIGHT HOLDER
# This file is distributed under the same license as the l package.
# Canonical OEM, 2010.
# 
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: ayatana-dev@lists.launchpad.net\n"
"POT-Creation-Date: 2022-12-31 13:18+0530\n"
"PO-Revision-Date: 2010-03-02 12:36-0500\n"
"Last-Translator: Canonical OEM\n"
"Language-Team: Portuguese\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid " (Hold)"
msgstr ""

msgid " (Tap)"
msgstr ""

msgid " + 1 to 9"
msgstr ""

msgid " + Arrow Keys"
msgstr ""

msgid " + Shift + 1 to 9"
msgstr ""

msgid " Drag"
msgstr ""

msgid " or Right"
msgstr ""

#. The "%s" is used in the dash preview to display the "<hint>: <value>" infos
#, c-format
msgid "%s:"
msgstr ""

msgid "Add to Dash"
msgstr ""

msgid "All"
msgstr ""

msgid "Arrow Keys"
msgstr ""

msgid "Caps lock is on"
msgstr ""

msgid "Categories"
msgstr ""

msgid "Close"
msgstr ""

msgid "Closes the current window."
msgstr ""

msgid "Closes the selected application / window."
msgstr ""

msgid "Comments"
msgstr ""

msgid "Ctrl + Alt + Num (keypad)"
msgstr ""

msgid "Ctrl + Tab"
msgstr ""

msgid "Cursor Left or Right"
msgstr ""

msgid "Cursor Up or Down"
msgstr ""

msgid "Dash"
msgstr ""

msgid "Drop To Add Application"
msgstr ""

msgid "Eject"
msgstr ""

msgid "Eject parent drive"
msgstr ""

msgid "Empty Trash…"
msgstr ""

msgid "Enter"
msgstr ""

msgid "Enter / Exit from spread mode or Select windows."
msgstr ""

msgid "Exit"
msgstr ""

msgid "Filter results"
msgstr ""

msgid "Format…"
msgstr ""

msgid "HUD"
msgstr ""

msgid "HUD & Menu Bar"
msgstr ""

msgid "Installing…"
msgstr ""

msgid "Keyboard Shortcuts"
msgstr ""

msgid "Last Updated"
msgstr ""

msgid "Launcher"
msgstr ""

msgid "Left Mouse"
msgstr ""

#, fuzzy
msgid "Lock to Launcher"
msgstr "Remover do Launcher"

msgid "Maximises the current window."
msgstr ""

msgid "Middle Mouse"
msgstr ""

msgid "Minimises all windows."
msgstr ""

msgid "Moves focus between indicators."
msgstr ""

msgid "Moves focused window to another workspace."
msgstr ""

msgid "Moves the focus."
msgstr ""

msgid "Moves the window."
msgstr ""

msgid "Multi-range"
msgstr ""

msgid "No Image Available"
msgstr ""

msgid "Open"
msgstr ""

msgid "Opens Launcher keyboard navigation mode."
msgstr ""

msgid "Opens a new window in the app."
msgstr ""

msgid "Opens the Dash App Lens."
msgstr ""

msgid "Opens the Dash Files Lens."
msgstr ""

msgid "Opens the Dash Home."
msgstr ""

msgid "Opens the Dash Music Lens."
msgstr ""

msgid "Opens the Dash Photo Lens."
msgstr ""

msgid "Opens the Dash Video Lens."
msgstr ""

msgid "Opens the HUD."
msgstr ""

msgid "Opens the Launcher, displays shortcuts."
msgstr ""

msgid "Opens the Trash."
msgstr ""

msgid "Opens the currently focused item."
msgstr ""

msgid "Opens the indicator menu."
msgstr ""

msgid "Opens the window accessibility menu."
msgstr ""

msgid "Places the window in corresponding position."
msgstr ""

msgid "Quit"
msgstr "Abandonar"

msgid "Rating"
msgstr ""

msgid "Resizes the window."
msgstr ""

msgid "Restore Windows"
msgstr ""

msgid "Restores or minimises the current window."
msgstr ""

msgid "Reveals the application menu."
msgstr ""

msgid "Right Mouse"
msgstr ""

msgid "Safely remove"
msgstr ""

msgid "Safely remove parent drive"
msgstr ""

msgid "Same as clicking on a Launcher icon."
msgstr ""

msgid "Search your computer"
msgstr ""

msgid "Search your computer and online sources"
msgstr ""

msgid "See fewer results"
msgstr ""

msgid "Semi-maximise the current window."
msgstr ""

msgid "Show Desktop"
msgstr ""

msgid "Sorry, there is nothing that matches your search."
msgstr ""

msgid "Spreads all windows in all the workspaces."
msgstr ""

msgid "Spreads all windows in the current workspace."
msgstr ""

msgid "Spreads all windows of the focused application in all the workspaces."
msgstr ""

msgid ""
"Spreads all windows of the focused application in the current workspace."
msgstr ""

msgid "Spreads all windows of the focused application."
msgstr ""

msgid "Spreads all windows."
msgstr ""

msgid "Switches applications via the Launcher."
msgstr ""

msgid "Switches between Lenses."
msgstr ""

msgid "Switches between applications from all workspaces."
msgstr ""

msgid "Switches between applications."
msgstr ""

msgid "Switches between workspaces."
msgstr ""

msgid "Switches windows of current applications."
msgstr ""

msgid "Switches workspaces."
msgstr ""

msgid "Switching"
msgstr ""

msgid "Take a screenshot of the current window."
msgstr ""

msgid "Take a screenshot."
msgstr ""

msgid "The drive has been successfully ejected"
msgstr ""

msgid "Trash"
msgstr ""

#, fuzzy
msgid "Unlock from Launcher"
msgstr "Remover do Launcher"

msgid "Unmount"
msgstr ""

#. Application is being installed, or hasn't been installed yet
msgid "Waiting to install"
msgstr ""

msgid "Windows"
msgstr ""

#, c-format
msgid "Workspace %d"
msgstr ""

#, c-format
msgid "Workspace %dx%d"
msgstr ""

msgid "Workspace Switcher"
msgstr ""

msgid "Workspaces"
msgstr ""

#, fuzzy
#~ msgid "Hide Launcher"
#~ msgstr "Remover do Launcher"

#, fuzzy
#~ msgid "Keep in launcher"
#~ msgstr "Manter no Launcher"

#, fuzzy
#~ msgid "Launchers"
#~ msgstr "Remover do Launcher"
