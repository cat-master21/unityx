# UnityX

## For distribution maintainers and users

There are a couple of subprojects under the `unityx/` subdirectory - `plotinus` and `windowck-plugin` (both of which are forks of existing projects with
the same names, just with UnityX-specific changes). It's recommended to package these as separate packages (and I'll probably be moving them to separate
repos later). UnityX's dependency list has been moved to the bottom of this page, as it's pretty long.

## Configuration

UnityX will create a file named `~/.local/share/unity/unityx.yaml` with many configuration options, which you can modify. After
making any changes, you need to log out and log in (on-the-fly reloading will be added soon). Many settings can also be changed
from UnityX Control Center.

## Manual installation

Before proceeding with these steps, install all of the [dependencies](#dependencies) (including `required with default config`).

1. Navigate to the unityx directory, and install both `plotinus` and `windowck-plugin` (both contain installation instructions in their
   README.md files)
2. Install the patched version of Nux for UnityX from https://gitlab.com/ubuntu-unity/unity-x/nux.
3. Run: `mkdir build && cd build && cmake .. && make -j16 && sudo make install`
4. Install UnityX Control Center from https://gitlab.com/ubuntu-unity/unity-x/unityx-control-center.
5. Install UnityX Power Manager from https://gitlab.com/ubuntu-unity/unity-x/unityx-power-manager.
6. Voila! You should now see UnityX in the session list when logging in. You can now configure UnityX
   using the [config file](#configuration) and UnityX Control Center.

## Dependencies

* `appstream-glib`
* `atk`
* `atk-bridge-2.0`
* `cairo>=1.13.1`
* `dbusmenu-glib-0.4`
* `dee-1.0`
* `gio-2.0>=2.30.0`
* `gio-unix-2.0`
* `gmodule-2.0`
* `gthread-2.0`
* `gtk+-3.0>=3.1`
* `indicator3-0.4>=0.4.90`
* `json-glib-1.0`
* `libbamf3>=0.5.3`
* `gnome-desktop-3.0`
* `libunity-settings-daemon`
* `libnotify`
* `libstartup-notification-1.0`
* `nux-4.0>=4.0.5`
* `sigc++-2.0>=2.4.0`
* `unity-misc>=0.4.0`
* `zeitgeist-2.0`
* `libgeis`
* `x11`
* `xfixes`
* `xi>=1.6.99.1`
* `xrender>=0.9`
* `nemo`
* `appmenu-gtk3-module`
* `appmenu-registrar`
* `xfce4-panel` (required with default config)
* `xfce4-panel-appmenu`/`vala-panel-appmenu` built with `xfce4-panel` support (required with default config)
* `nm-applet` (required with default config)
* `unity-settings-daemon`
